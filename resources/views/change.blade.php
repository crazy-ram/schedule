<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Замена расписания</title>
</head>
<body>
<form action="change" method="post">
    Текущий урок <br>
    <select name="change" id="">
        <? foreach($schedule as $change): ?>
        <option value="<?= $change->id ?>"><?= "{$change->day_name}, {$change->group_number}, {$change->number} урок, {$change->lesson_name}, {$change->fio}, кабинет {$change->cabinet_number}" ?></option>
        <? endforeach; ?>
    </select>
    <br>
    Урок <br>
    <select name="lesson_teacher" id="">
        <? foreach($lesson_teacher as $l_t): ?>
        <option value="<?= $l_t->id ?>"><?= $l_t->lesson_name ?> - <?= $l_t->fio ?></option>
        <? endforeach; ?>
    </select> <br>
    Кабинет <br>
    <select name="cabinet" id="">
        <? foreach($cabinets as $cabinet): ?>
        <option value="<?= $cabinet->id ?>"><?= $cabinet->cabinet_number ?></option>
        <? endforeach; ?>
    </select>
    <br>
    Дата <br>
    <input type="date" name="date">
    <br>
    <input type="submit" value="Сохранить">
</form>
</body>
</html>
