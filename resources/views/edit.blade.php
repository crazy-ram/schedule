<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Изменить расписание</title>
</head>
<body>
<form action="edit?id=<?= $schedule->id ?>" method="post">
    День <br>
    <select name="day" id="">
        <? foreach($days as $day): ?>
        <option value="<?= $day->id ?>" <?= $day->id == $schedule->id_day ? 'selected' : '' ?>><?= $day->day_name ?></option>
        <? endforeach; ?>
    </select> <br>
    Группа <br>
    <select name="group" id="">
        <? foreach($groups as $group): ?>
        <option value="<?= $group->id ?>" <?= $group->id == $schedule->id_group ? 'selected' : '' ?>><?= $group->group_number ?></option>
        <? endforeach; ?>
    </select> <br>
    Номер урока <br>
    <select name="lessons_number" id="">
        <? foreach($lessons_number as $l_n): ?>
        <option value="<?= $l_n->id ?>" <?= $l_n->id == $schedule->id_lessons_number ? 'selected' : '' ?>><?= $l_n->number ?></option>
        <? endforeach; ?>
    </select> <br>
    Урок <br>
    <select name="lesson_teacher" id="">
        <? foreach($lesson_teacher as $l_t): ?>
        <option value="<?= $l_t->id ?>" <?= $l_t->id == $schedule->id_lesson_teacher ? 'selected' : '' ?>><?= $l_t->lesson_name ?> - <?= $l_t->fio ?></option>
        <? endforeach; ?>
    </select> <br>
    Кабинет <br>
    <select name="cabinet" id="">
        <? foreach($cabinets as $cabinet): ?>
        <option value="<?= $cabinet->id ?>" <?= $cabinet->id == $schedule->id_cabinet ? 'selected' : '' ?>><?= $cabinet->cabinet_number ?></option>
        <? endforeach; ?>
    </select>
    <br>
    <input type="submit" value="Сохранить">
</form>
</body>
</html>
