<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Расписание группы {{ $group_number }}</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
<div class="container">
    <div class="group-list">
        <span class="group-btn"><strong>Список групп</strong></span>
        <ul class="dropdown hidden">
            <?php
            foreach($groups as $group) {
                echo "<li class='group'><a href='?group={$group->id}&day={$day_id}'>{$group->group_number}</a></li>";
            }
            ?>
        </ul>
    </div>

    <div class="schedule">
        <div class="schedule-title">
            <h1>Расписание группы {{ $group_number }}</h1>
            <h3>
                <?php
                $arr = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
                $month = $arr[date('n')-1];
                echo date("d {$month} Y г.");
                ?>
            </h3>
        </div>
        <ul class="schedule-days">
            <?php
            foreach($days as $day) {
                echo $day_id == $day->id ? "<li class='days'><a class='active' href='?group={$group_id}'>{$day->day_name}</a></li>" : "<li class='days'><a href='?group={$group_id}&day={$day->id}'>{$day->day_name}</a></li>";
            }
            ?>
        </ul>

        <table class="schedule-table">
            <tr class="schedule-header">
                <td class="day">День</td>
                <td class="time">Время</td>
                <td class="cabinet">Кабинет</td>
                <td class="lesson">Предмет</td>
                <td class="teacher">Преподаватель</td>
            </tr>
        </table>

        <? foreach($sortedSchedule as $day): ?>
           <table class="schedule-table">
               <? foreach ($day as $lesson): ?>
                    <tr class='schedule-day <?= $lesson->status ? 'change' : '' ?> '>
                        <th class='day'><span> <?= $lesson->day_name ?> </span></th>
                        <td class='time'> <?= $lesson->time ?> </td>
                        <td class='cabinet'> <?= $lesson->cabinet_number ?> </td>
                        <td class='lesson'> <?= $lesson->lesson_name ?> </td>
                        <td class='teacher'> <?= $lesson->fio ?> </td>
                    </tr>
               <? endforeach; ?>
            </table>
        <? endforeach; ?>
    </div>
</div>
<script src="js/index.js"></script>
</body>
</html>
