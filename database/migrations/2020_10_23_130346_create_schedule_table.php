<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('id_day')->index('id_day');
            $table->integer('id_lesson_teacher')->index('id_lesson-teacher');
            $table->integer('id_group')->index('id_group');
            $table->integer('id_lessons_number')->index('id_lessons-number');
            $table->integer('id_cabinet')->index('id_cabinet');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
