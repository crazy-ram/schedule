<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule', function (Blueprint $table) {
            $table->foreign('id_group', 'schedule_ibfk_1')->references('id')->on('groups')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_lesson_teacher', 'schedule_ibfk_2')->references('id')->on('lesson-teacher')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_lessons_number', 'schedule_ibfk_3')->references('id')->on('lessons-number')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_day', 'schedule_ibfk_4')->references('id')->on('days')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_cabinet', 'schedule_ibfk_5')->references('id')->on('cabinets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule', function (Blueprint $table) {
            $table->dropForeign('schedule_ibfk_1');
            $table->dropForeign('schedule_ibfk_2');
            $table->dropForeign('schedule_ibfk_3');
            $table->dropForeign('schedule_ibfk_4');
            $table->dropForeign('schedule_ibfk_5');
        });
    }
}
