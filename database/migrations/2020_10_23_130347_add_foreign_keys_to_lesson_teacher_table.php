<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToLessonTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson-teacher', function (Blueprint $table) {
            $table->foreign('id_lesson', 'lesson-teacher_ibfk_1')->references('id')->on('lessons')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_teacher', 'lesson-teacher_ibfk_2')->references('id')->on('teachers')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson-teacher', function (Blueprint $table) {
            $table->dropForeign('lesson-teacher_ibfk_1');
            $table->dropForeign('lesson-teacher_ibfk_2');
        });
    }
}
