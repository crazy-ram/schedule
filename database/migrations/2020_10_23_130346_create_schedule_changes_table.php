<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule-changes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->date('date');
            $table->integer('id_schedule')->index('id_schedule');
            $table->integer('id_lesson_teacher')->index('id_lesson-teacher');
            $table->integer('id_cabinet')->index('id_cabinet');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule-changes');
    }
}
