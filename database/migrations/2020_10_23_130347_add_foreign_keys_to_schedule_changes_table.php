<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToScheduleChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule-changes', function (Blueprint $table) {
            $table->foreign('id_schedule', 'schedule-changes_ibfk_1')->references('id')->on('schedule')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_lesson_teacher', 'schedule-changes_ibfk_2')->references('id')->on('lesson-teacher')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('id_cabinet', 'schedule-changes_ibfk_3')->references('id')->on('cabinets')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule-changes', function (Blueprint $table) {
            $table->dropForeign('schedule-changes_ibfk_1');
            $table->dropForeign('schedule-changes_ibfk_2');
            $table->dropForeign('schedule-changes_ibfk_3');
        });
    }
}
