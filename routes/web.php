<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    $group_id = $request->get('group');
    $day_id = $request->get('day');

    if(!isset($group_id)) $group_id = 1;

    if (isset($day_id)) {
        $schedule = DB::table('schedule')
            ->where('id_group', $group_id)
            ->where('id_day', $day_id)
            ->join('days', 'schedule.id_day', 'days.id')
            ->join('lesson-teacher', 'schedule.id_lesson_teacher', 'lesson-teacher.id')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->join('lessons-number', 'schedule.id_lessons_number', 'lessons-number.id')
            ->join('cabinets', 'schedule.id_cabinet', 'cabinets.id')
            ->select('schedule.id', 'schedule.id_day', 'days.day_name', 'teachers.fio', 'lessons.lesson_name', 'lessons-number.number', 'lessons-number.time',  'cabinets.cabinet_number')
            ->orderBy('lessons-number.number', 'asc')
            ->orderBy('days.id', 'asc')
            ->get();
    } else {
        $schedule = DB::table('schedule')
            ->where('id_group', $group_id)
            ->join('days', 'schedule.id_day', 'days.id')
            ->join('lesson-teacher', 'schedule.id_lesson_teacher', 'lesson-teacher.id')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->join('lessons-number', 'schedule.id_lessons_number', 'lessons-number.id')
            ->join('cabinets', 'schedule.id_cabinet', 'cabinets.id')
            ->select('schedule.id', 'schedule.id_day', 'days.day_name', 'teachers.fio', 'lessons.lesson_name', 'lessons-number.number', 'lessons-number.time',  'cabinets.cabinet_number')
            ->orderBy('lessons-number.number', 'asc')
            ->orderBy('days.id', 'asc')
            ->get();
    }

    $sortedSchedule = [];

    foreach ($schedule as $lesson) {
        $lesson->status = false;


        $currentDate = date("Y-m-d");

        $change = DB::table('schedule-changes')
            ->where('id_schedule', $lesson->id)
            ->where('date', '>=', $currentDate)
            ->join('lesson-teacher', 'schedule-changes.id_lesson_teacher', 'lesson-teacher.id')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->join('cabinets', 'schedule-changes.id_cabinet', 'cabinets.id')
            ->select('teachers.fio', 'lessons.lesson_name', 'cabinets.cabinet_number')
            ->first();

        if(!empty($change)) {
            $lesson->fio = $change->fio;
            $lesson->cabinet_number = $change->cabinet_number;
            $lesson->lesson_name = $change->lesson_name;
            $lesson->status = true;
        }

        $sortedSchedule[$lesson->id_day][] = $lesson;
    }

    $days = DB::table('days')->get();
    $groups = DB::table('groups')->get();
    $group_number = DB::table('groups')->where('id', $group_id)->first()->group_number;

    return view('welcome', compact(['sortedSchedule', 'days', 'groups', 'day_id', 'group_id', 'group_number']));
});

Route::middleware(['basicAuth'])->group(function () {
    Route::get('/add', function () {
        $days = DB::table('days')->get();
        $lesson_teacher = DB::table('lesson-teacher')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->select('lesson-teacher.id', 'teachers.fio', 'lessons.lesson_name')
            ->get();
        $groups = DB::table('groups')->get();
        $lessons_number = DB::table('lessons-number')->get();
        $cabinets = DB::table('cabinets')->get();

        return view('add', compact(['days', 'lesson_teacher', 'groups', 'lessons_number', 'cabinets']));
    });

    Route::post('/add', function (Request $request) {
        $id = DB::table('schedule')->insertGetId(array(
            'id_day' => $request->post('day'),
            'id_lesson_teacher' => $request->post('lesson_teacher'),
            'id_group' => $request->post('group'),
            'id_lessons_number' => $request->post('lessons_number'),
            'id_cabinet' => $request->post('cabinet')
        ));

        return redirect('/');
    });

    Route::get('/edit', function(Request $request) {
        $id = $request->get('id');
        if(empty($id)) return redirect('/');

        $schedule = DB::table('schedule')->where('id', $id)->first();

        $days = DB::table('days')->get();
        $lesson_teacher = DB::table('lesson-teacher')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->select('lesson-teacher.id', 'teachers.fio', 'lessons.lesson_name')
            ->get();
        $groups = DB::table('groups')->get();
        $lessons_number = DB::table('lessons-number')->get();
        $cabinets = DB::table('cabinets')->get();

        if(empty($schedule)) return redirect('/');

        return view('edit', compact(['schedule', 'days', 'lesson_teacher', 'groups', 'lessons_number', 'cabinets']));
    });

    Route::post('/edit', function (Request $request) {
        $id = DB::table('schedule')
            ->where('id', $request->get('id'))
            ->update(array(
            'id_day' => $request->post('day'),
            'id_lesson_teacher' => $request->post('lesson_teacher'),
            'id_group' => $request->post('group'),
            'id_lessons_number' => $request->post('lessons_number'),
            'id_cabinet' => $request->post('cabinet')
        ));

        return redirect('/');
    });

    Route::get('/change', function() {
        $schedule = DB::table('schedule')
            ->join('days', 'schedule.id_day', 'days.id')
            ->join('lesson-teacher', 'schedule.id_lesson_teacher', 'lesson-teacher.id')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->join('lessons-number', 'schedule.id_lessons_number', 'lessons-number.id')
            ->join('cabinets', 'schedule.id_cabinet', 'cabinets.id')
            ->join('groups', 'schedule.id_group', 'groups.id')
            ->select('schedule.id', 'schedule.id_day', 'days.day_name', 'teachers.fio', 'lessons.lesson_name', 'lessons-number.number', 'lessons-number.time',  'cabinets.cabinet_number', 'groups.group_number')
            ->orderBy('lessons-number.number', 'asc')
            ->orderBy('days.id', 'asc')
            ->get();

        $lesson_teacher = DB::table('lesson-teacher')
            ->join('teachers', 'lesson-teacher.id_teacher', 'teachers.id')
            ->join('lessons', 'lesson-teacher.id_lesson', 'lessons.id')
            ->select('lesson-teacher.id', 'teachers.fio', 'lessons.lesson_name')
            ->get();
        $cabinets = DB::table('cabinets')->get();

        return view('change', compact(['schedule', 'lesson_teacher', 'cabinets']));
    });

    Route::post('/change', function (Request $request) {
        $id = DB::table('schedule-changes')->insertGetId(array(
            'id_schedule' => $request->post('change'),
            'id_lesson_teacher' => $request->post('lesson_teacher'),
            'id_cabinet' => $request->post('cabinet'),
            'date' => $request->post('date')
        ));

        return redirect('/');
    });
});
